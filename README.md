# Corso di Analisi della Musica Elettroacustica (Nicola Bernardini)

Repository del corso di Analisi della Musica Elettroacustica (COME-02), tenuto
da Nicola Bernardini dall'A.A.2022-2023.

Il programma complessivo del corso è [qui](./programma.md)

# LICENZA

[<img
  src=http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png
  width=88
  alt="CreativeCommons Attribution-ShareAlike 4.0"
/>](./LICENSE)

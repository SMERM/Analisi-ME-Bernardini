# Corso di Analisi della Musica Elettroacustica (Bernardini) - A.A.2022-2023

## Analisi degli studenti

### Leonardo Saba

Luigi Nono, *A Pierre, nell'azzurro. Inquietum*
Jonathan Harvey, *Mortuos Plango, vivum voco*

### Francesco Vesprini

Ludwig Van Beethoven, *Sonata Waldstein* (primo movimento)
Curtis Roads, uno da *Point-Line-Cloud*

### Luca Simone

Luciano Berio, *Lied* per clarinetto
Dennis Smalley, *Wind Chimes* 

### Filippo Fossà

Barry Truax, *Riverrun*

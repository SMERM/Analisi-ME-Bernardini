# Corso di Analisi della Musica Elettroacustica (Nicola Bernardini) - Lezione n.2 18/05/2023

### La Pratica analitica

#### Segmentazione del Materiale

* Basso livello
  * segmentazione tonale
    * armonia
    * melodia
  * segmentazione atonale
  * segmentazione dodecafonica
  * segmentazione seriale
  * segmentazione elettroacustica

* Alto livello
  * segmentazione formale
  * inserimento nel contesto storico

## Esempio di Analisi

Berio, *Différences* (1959)

* ascolto
* introduzione
* segmentazione del materiale

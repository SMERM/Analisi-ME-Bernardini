# Corso di Analisi della Musica Elettroacustica (Nicola Bernardini) - Lezione n.1 11/05/2023

## Introduzione all'analisi musicale

### Fondamenti teorici dell'Analisi Musicale

* Pratiche analitiche:
  * Storia dell'analisi musicale
* Funzioni dell'analisi musicale
  * musicologica
  * didattica
  * semiotica
* Cosa *non è* analisi musicale (descrizioni, programmi di sala, critica
  musicale, ecc.)

### La Pratica analitica

#### Segmentazione del Materiale

* Basso livello
  * segmentazione tonale
    * armonia
    * melodia
  * segmentazione atonale
  * segmentazione dodecafonica
  * segmentazione seriale
  * segmentazione elettroacustica

* Alto livello
  * segmentazione formale
  * inserimento nel contesto storico

#### Estrazione del senso

* Elementi di semiosi
  * livello poietico
  * livello estesico
  * livello neutro
  * sovrapposizioni strutturali

### Esercizi di analisi

#### Analisi di un lavoro strumentale

#### Analisi di due lavori elettroacustici
  * acusmatico (partitura d'ascolto, ecc.)
  * misto

## Esempio di Analisi

Berio, *Différences* (1959)

## Analisi possibili

### Brani tradizionali

Berio, *Lied*
Berio, *Brin*
Mozart, *Sonata quasi una fantasia* K.475
Beethoven, sonata *Waldstein* (primo movimento)

### Brani Elettroacustici acusmatici

Risset, *Sud* (primo movimento)
Harvey, *Mortuos plango vivum voco*
Stockhausen, *Gesang der Junglinge*
Berio, *Visage*

### Brani Elettroacustici misti

Berio, *Differences*

## Lavagna

![whiteboard 1](./Analisi Musicale 11-05-2023 13. 00.jpg)

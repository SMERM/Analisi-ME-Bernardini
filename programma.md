# Corso di Analisi della Musica Elettroacustica (Nicola Bernardini)

## Fondamenti teorici dell'Analisi Musicale

* Pratiche analitiche:
  * Storia dell'analisi musicale
* Funzioni dell'analisi musicale
  * musicologica
  * didattica
  * semiotica
* Cosa *non è* analisi musicale (descrizioni, programmi di sala, critica
  musicale, ecc.)

## La Pratica analitica

### Segmentazione del Materiale

* Basso livello
  * segmentazione tonale
    * armonia
    * melodia
  * segmentazione atonale
  * segmentazione dodecafonica
  * segmentazione seriale
  * segmentazione elettroacustica

* Alto livello
  * segmentazione formale
  * inserimento nel contesto storico

### Estrazione del senso

* Elementi di semiosi
  * livello poietico
  * livello estesico
  * livello neutro
  * sovrapposizioni strutturali

## Esercizi di analisi

### Analisi di un lavoro strumentale

### Analisi di due lavori elettroacustici
  * acusmatico (partitura d'ascolto, ecc.)
  * misto

## Bibliografia

* Bent, Ian and Drabkin, William *Analisi Musicale*, 1990, Manuali EDT/SIdM
* Forte, Allen *The structure of atonal music*
* Perle, George *Serial composition and atonality*
